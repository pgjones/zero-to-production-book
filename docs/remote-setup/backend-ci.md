As a [reminder](../local-setup/backend-tooling.md) the commands to
check the formatting, lint, and test are,

```shell
poetry run format
poetry run lint
poetry run test
```

To have these run as part of CI i.e. for every change to the remote
repository the `.gitlab-ci.yml` file placed in the root of the
repository should contain,

```yaml
backend-ci:
  image: python:3.9-alpine

  cache:
    key: poetry-cache
    paths:
      - backend/.venv

  before_script:
  - apk --update add alpine-sdk cargo gcc libffi-dev musl-dev postgresql-client
  - pip install poetry
  - poetry config virtualenvs.in-project true
  - cd backend
  - poetry install

  services:
  - postgres

  variables:
    POSTGRES_DB: tozo_test
    POSTGRES_USER: tozo
    POSTGRES_PASSWORD: tozo
    POSTGRES_HOST_AUTH_METHOD: "trust"

  script:
  - poetry run format
  - poetry run lint
  - poetry run test_ci

  only:
    changes:
      - backend/**/*
      - .gitlab-ci.yml
```

The postgres service has been added to allow for testing against the
database in CI.

## Security checks

!!! warning

    The safety tool described in this section requires a [license for
    commercial usage](https://pyup.io/safety/).

It is good practice to regularly update any dependencies used, in
order to get the latest security fixes and updates. Alongside this it
helps to regularly check if there are known security issues, which can
be done via the safety tool,

```shell
safety check
```

Safety can be run periodically using a Gitlab-CI schedule, by adding
the following to the `.gitlab-ci.yml` file placed in the root of the
repository,

```yaml
backend-audit:
  image: python:3.9-alpine

  before_script:
  - pip install safety

  script:
  - safety check

  only:
  - schedules
```

which will run on the schedule defined in the [ci
setup](../remote-setup/ci.md).
