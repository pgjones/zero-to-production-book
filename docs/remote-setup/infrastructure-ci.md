As a [reminder](../local-setup/infrastructure-tooling.md) the commands to
check the formatting and lint are,

```shell
terraform fmt --check=true
terraform validate
```

To have these run as part of CI i.e. for every change to the remote
repository a `.gitlab-ci.yml` file placed in the root of the
repository should contain,

```yaml
infrastructure-ci:
  image: alpine:latest

  before_script:
  - apk update
  - apk add terraform
  - cd infrastructure
  - terraform init

  script:
  - terraform fmt --check=true
  - terraform validate

  only:
    changes:
      - infrastructure/**/*
      - .gitlab-ci.yml
```
