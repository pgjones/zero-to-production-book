![Gitlab logo](img/gitlab.png){: style="height: 80px;float: right"}

A remote repository acts as a backup for all your code and makes it
much easier to setup continuous integration, CI (testing, linting
etc). We'll use [gitlab](https://gitlab.com/), although you could also
use github.

Rather than creating the repository through gitlab's UI we'll use
Terraform as [setup previously](../local-setup/infrastructure.md). To
do so we'll first need an [access
token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html).
This token is a secret, and hence best placed in the
`infrastructure/secrets.auto.tfvars` as so,

```terraform
gitlab_token = "abc1234"
```

We can then instruct terraform to use the gitlab provider to create a
gitlab project, called `tozo` (change as appropriate :smile:), by
adding the following to `infrastructure/main.tf`,

```terraform
terraform {
  required_providers {
    gitlab = {
      source  = "gitlabhq/gitlab"
      version = ">=3.3.0"
    }
  }
  required_version = ">=0.14"
}
```

to configure terraform with gitlab, and then the following to
`infrastructure/gitlab.tf`,

```terraform
variable "gitlab_token" {
  sensitive = true
}

provider "gitlab" {
  token = var.gitlab_token
}

resource "gitlab_project" "tozo" {
  name             = "tozo"
  visibility_level = "private"
  default_branch   = "main"
}
```

which you can then apply via following commands,

```shell
terraform init
terraform apply
```

## Committing and pushing

Now we've run these commands we should encrypt the [secrets and
state](../local-setup/infrastructure.md) as both have now changed,

```shell
ansible-vault encrypt secrets.auto.tfvars --output=secrets.auto.tfvars.vault
ansible-vault encrypt terraform.tfstate --output=terraform.tfstate.vault
```

which should result in these useful files in the repository,

```
tozo
├── backend
├── frontend
└── infrastructure
    ├── .gitignore
    ├── ansible.cfg
    ├── gitlab.tf
    ├── main.tf
    ├── secrets.auto.tfvars
    ├── secrets.auto.tfvars.vault
    ├── terraform.tfstate
    └── terraform.tfstate.vault
```

so lets commit those (excluding `secrets.auto.tfvars` and
`terraform.tfstate`) and push to the remote (with your namespace and
project names),

```shell
git remote add origin git@gitlab.com:namespace/tozo
git push origin main
```
