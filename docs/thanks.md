With thanks to the following people who have kindly given feedback and
advice on this book,

 - [@ManuelaRE](https://gitlab.com/ManuelaRE)
 - [@farhaan-ali](https://gitlab.com/farhaan-ali)
 - [@richardsheridan](https://gitlab.com/richardsheridan)
 - [@philberesford](https://github.com/philberesford)
 - [@khamiltonuk](https://gitlab.com/khamiltonuk)
