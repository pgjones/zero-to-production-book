So far in development we've used `app.run` to run and serve the
backend. Whilst this works well, it is explicitly not meant for
production. Rather the
[ASGI](https://asgi.readthedocs.io/en/latest/introduction.html) server
[Hypercorn](https://gitlab.com/pgjones/hypercorn) is recommended by
Quart. It is also something I know very well as I'm the Hypercorn
author.

To use Hypercorn in the production environment we need to configure
how it should work. I prefer to do this via a [TOML](https://toml.io)
configuration file. At this stage we should configure the logging, and
restrict the host names the server will respond to by adding the
following to `hypercorn.toml`,

```toml
accesslog = "-"
access_log_format = "%(t)s %(h)s %(f)s - %(S)s '%(r)s' %(s)s %(b)s %(D)s"
errorlog = "-"
server_names = [ "tozo.dev", "www.tozo.dev" ]
```

!!! info

    Setting the `serve_names` means that the server will respond with
    a 404, not found response to any request that targets a different
    host that those given.

The hypercon server can be started via the following command,

```sh
hypercorn --config hypercorn.toml 'run:create_app()'
```

which we'll add to a short script `backend/start`,

```sh
#!/bin/sh
hypercorn --bind 0.0.0.0:$PORT --config hypercorn.toml 'backend.run:create_app()'
```

note that the port is set using an environment variable, as this will
help us configure the server in Heroku.
