This is the tech and tech stack I developed starting my own
startup. My aim was to build high quality apps as quickly as possible
in the languages and tooling I was familiar with.

I'm hoping this proves to be a useful resource to junior engineers to
see how a complete and deployed production app is developed. Ideally
this will take a beginner from nothing to their own production app.

To know more about me please visit
[pgjones.dev](https://pgjones.dev). I am the author of
[Quart](https://gitlab.com/pgjones/quart) (one of the frameworks used
in this book) and a maintainer of Pallets (Flask, Werkzeug) and Hyper
(h2, h11) projects.
