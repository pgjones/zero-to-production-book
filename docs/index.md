The aim of this book is to build an app and run it in a production
environment so that you can do the same. We'll build an app packed
full of features that you can use in your app, and a few todo features
as the app will need to do something.

I've built the app described in this book, and it is running at
[tozo.dev](https://tozo.dev) with the code available
[here](https://gitlab.com/pgjones/tozo).

I'm going to start by assuming you have no tooling installed but that
you have a working knowledge of
[Typescript](https://www.typescriptlang.org/) and
[Python](https://www.python.org/). If you know more I'd just skip
ahead to any of the sections that interest you, if you know less I'd
seek to learn the basics of these languages first.

## Aims

Whilst you are building an app to solve your customers' specific
problems many of the techniques will be the same as in the app built
here; for example, users will need to login, change their password etc
regardless. Therefore the todo app built in this book will utilise as
many of the techniques I'd expect you to need for your own app.

## Reading this book

I mean for this book to be read in the order given in the navigation,
however you can jump around as you see fit. The companion
[repository](https://gitlab.com/pgjones/tozo) is also ordered this way
with each commit roughly corresponding to a page in this book.

If you have any comments, suggestions, or corrections please add an
[issue](https://gitlab.com/pgjones/zero-to-production-book/-/issues).

## Tech Choices

It should be noted that many/most of the choices made are my personal
preferences and that there are equally good alternatives. You can
either follow me or take the parts that interest you.

As mentioned above I'm starting by assuming you have a working
knowledge of Typescript and Python i.e. you can follow along with code
snippets in these languages.

### Typescript

We will want our app to run in the browser, which means whatever
language we choose it will need to compile to Javascript to run. To me
Typescript is Javascript with additional type safety, which makes it
an easy choice as I like Javascript and I find the type safety very
helpful.

### Python + typing

For the parts of the app that run on the server we have a much wider
choice of languages. I'm very familiar with, and contribute to, Python
so it is the obvious choice for me. As with Typescript though I find
the additional type safety from Python's type hinting really helpful,
so we'll use that as well.

## Naming the app

You'll hear that naming things is hard, and naming this app is no
different. An early draft of this book called the app `project` which
was a little to generic and boring. Therefore `tozo` is our new name
(from a combination of todo and zero to production).
