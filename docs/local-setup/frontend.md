![Node logo](img/node.png){: style="height: 80px;float: right"}

The frontend will be written in Typescript, so we need
[Node](https://nodejs.org/) installed to run it. To install node,

!!! info inline end ""

    Run this command in any location

```shell
brew install node
```

which installed Node 16.2.0.

As with Python brew doesn't know how to install javascript/typescript
packages fortunately node comes with [npm](https://www.npmjs.com/)
which is the default pacakge manager for JS/TS.

![Create React App logo](img/cra.png){: style="height: 80px;float: right"}

## Creating a React App

As with the backend we'll separate the frontend code into a frontend
folder but we'll use the [create react
app](https://create-react-app.dev/) tool to set everything up by
running this command in the project directory,

!!! info inline end ""

    Run this command in `tozo/`

```shell
npx create-react-app frontend --template typescript
```

which should give the following folder structure,

```
tozo
├── backend
│   └── ...
└── frontend
    ├── .gitignore
    ├── README.md
    ├── node_modules
    ├── package.json
    ├── package-lock.json
    ├── tsconfig.json
    ├── public
    │   ├── favicon.ico
    │   ├── index.html
    │   ├── logo192.png
    │   ├── logo512.png
    │   ├── manifest.json
    │   └── robots.txt
    └── src
        ├── App.css
        ├── App.tsx
        ├── App.test.tsx
        ├── index.css
        ├── index.tsx
        ├── logo.svg
        ├── react-app-env.d.ts
        ├── reportWebVitals.ts
        └── setupTests.js
```

only the `package.json`, `package-lock.json`, `tsconfig.json`,
`.gitignore`, `react-app-env.d.ts`, and `index.html` files matter at
the moment, so you can delete or adapt the other files as you'd like.
