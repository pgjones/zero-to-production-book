Unlike the frontend and backend code there is no tool to write scripts
in, instead the direct terraform commands should be used.

## Formatting

Terraform comes with an in-build formatter which can be invoked to
autoformat your code,

!!! info inline end ""

    Run this command in `infrastructure/`

```shell
terraform fmt
```

and to check if your code meets the formatting rules,

!!! info inline end ""

    Run this command in `infrastructure/`

```shell
terraform fmt --check=true
```

## Linting

Terraform also comes with a tool to check your code,

!!! info inline end ""

    Run this command in `infrastructure/`

```shell
terraform validate
```

## Testing

I currently don't know of a good technique to test the Terraform code,
other than running

!!! info inline end ""

    Run this command in `infrastructure/`

```shell
terraform plan
```

and checking the output makes sense.

!!! note

    These commands won't work or do much yet, as we have no terraform
    files to format, lint, or test.
