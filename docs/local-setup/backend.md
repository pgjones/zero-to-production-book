![Python logo](img/python.png){: style="height: 80px;float: right"}

The backend will be written in Python, so we need Python installed to
run it locally. You may already have a version of Python installed,
but we need to use Python>=3.9 so I'd recommend installing,

!!! info inline end ""

    Run this command in any location

```shell
brew install python
```

which installed Python 3.9.0.

![Poetry logo](img/poetry.png){: style="height: 80px;float: right"}

## Creating a backend project

Brew, the package manager we've used so far, doesn't know how to
install and manage Python packages. So we also need another package
manager. There are many choices in Python, and I think
[Poetry](https://python-poetry.org/) is the best, so lets install,

!!! info inline end ""

    Run this command in any location

```shell
brew install poetry
```

which installed Poetry 1.1.0.

We'll keep the backend code separate in a backend folder, so please
create it at the top level of the project and then create a new poetry
project within that folder,

!!! info inline end ""

    Run this command in `tozo/`

```shell
poetry new --src backend
```

which should give the following folder structure,

```
tozo
└── backend
    ├── pyproject.toml
    ├── README.rst
    ├── src
    │   └── backend
    │       └── __init__.py
    └── tests
        ├── __init__.py
        └── test_backend.py
```

only the `pyproject.toml` and folder structure matter at the moment, so
you can delete or adapt the other files as you'd like.
