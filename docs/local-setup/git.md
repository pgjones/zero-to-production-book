![Git](img/git.png){: style="height: 80px;float: right"}

If you've not done much development before (or maybe never developed
in a team) it may not be clear why you'd need git or a version control
system. I suspect though you are already using a version control
system, which if you are like me is just renaming files
e.g. `main_old.py` or `main_v2.py`. The problem is that your local
system is unique to you and probably offers no tooling to make it
easier or to share changes with others.

git solves these issues and is the best version control system, as
most modern tooling is built around it.

!!! note

    git can be very confusing to use, and you may end up with your
    repository in a mess. It does get easier with practice and there
    is plenty of help online. You can always delete your local
    repository and start again from the remote version (as I have many
    times).

You may already have git installed, try `git --version` to find out.
If you don't lets install it now,

!!! info inline end ""

    Run this command in any location

```shell
brew install git
```

which installed 2.29.2.

Now you have git installed lets create a repository for all our code,
First lets create a directory for our app, which we'll call `tozo` and
within which run,

!!! info inline end ""

    Run this command in `tozo/`

```shell
git init .
```

to create a git repository and by consequence add a `.git` to your
project structure,

```
tozo
└── .git
```

that we can now ignore :).

!!! tip "Clear atomic commits"

    Crafting atomic and well reasoned commits from the start will save
    you a great deal of time in the future. I've written about
    [this](https://smarketshq.com/the-importance-of-a-well-reasoned-commit-1b73dc498455)
    before.
