Our users will log in by providing their email and password which
means we need to manage their password in terms of storage and
strength.

## Password storage

Will will check the password provided at login against a password we
store when they create their account. This password must not be stored
directly as if an attacker gains access to the database they likely
gain the user's password to many other websites. Therefore instead of
the password a hash of the password is calculated and stored.

[bcrypt](https://github.com/pyca/bcrypt/) is a great library to hash
passwords. It can be installed,

!!! info inline end ""

    Run this command in `backend/`

```shell
poetry add bcrypt
```

## Password strength

We'll want to enforce a minimum password complexity to help ensure our
user's information is safe. To do this I use
[zxcvbn](https://github.com/dwolfhub/zxcvbn-python) to measure the
complexity. It is installed via poetry,

!!! info inline end ""

    Run this command in `backend/`

```shell
poetry add zxcvbn
```

and gives a score [0, 4] of which I usually consider scores of 3 or 4
good enough.
