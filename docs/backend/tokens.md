We'll want to send links to users that they can use to prove they are
a certain user. For example to change a forgotten password we can send
a link to the user's email which they can use to reset the
passwor. This works by including a token that identifies the user in
the link.

To create the token we can sign the user's ID using a cryptographic
hash function, secret key, and salt. This will ensure that a malicious
user cannot alter the token without generating a correct signature,
which requires our secret key. To do this we can use
[itsdangerous](https://itsdangerous.palletsprojects.com/), which is
installed via poetry,

!!! info inline end ""

    Run this command in `backend/`

```shell
poetry add itsdangerous
```

The downside to this approach is that users will be able to read
anything we place in the token. It is important therefore not to put
anything sensitive in the token (the user ID is not sensitive).

!!! note

    We should ensure we use a different salt for each token flow so
    that a malicious user cannot use a token generated for one flow in
    another. We should also encode the date the token was generated
    and reject tokens that are too old, as this will protect users if
    a malicious user gets hold of old emails/tokens.
