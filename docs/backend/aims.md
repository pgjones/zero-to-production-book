This project is to build a todo app that stores and allows editing of
a user's todos. Therefore the backend needs to provide an API to

 - Create, Read, Update, and Delete (CRUD) TODOs,
 - Allow users to register, and delete accounts,
 - Authenticate users, including login (including multi-factor
   authentication), logout, change and reset passwords,
 - Serve the frontend Single Page App (SPA),
 - Serve HTTP to HTTPS redirects to serve only over HTTPS.

## Tech choices

![Quart logo](img/quart.png){: style="height: 80px;float: right"}

To do all this I choose to use
[Quart](https://gitlab.com/pgjones/quart) as the base framework with
various Quart-extensions to provide extra functionality. Quart is the
[Flask](https://flask.palletsprojects.com) API re-implemented using
async/await which I know it very well as I'm the Quart
author. Alternatively you can adapt the code in this book to use Flask
and Flask-extensions without too much difficulty.
