Error responses are usually signified by the status code (being in the
400-500 range). However the status code alone cannot always convey
enough information, for example when registering a new member a status
code of 400 is expected for a request with an invalid email address
_and_ for a request with weak password. Hence there is a need to
return an additional code for these cases.

As the frontend expects JSON this is best acheived by a custom error
handler and exception class. The exception class is defined below and
should be added to `backend/src/backend/lib/api_error.py`,

```python
class APIError(Exception):
    def __init__(self, status_code: int, code: str) -> None:
        self.status_code = status_code
        self.code = code
```

the error handler should be added to `backend/src/backend/run.py`

```python
...
from backend.lib.api_error import APIError

def create_app() -> Quart:
    ...

    @app.errorhandler(APIError)  # type: ignore
    async def handle_api_error(error: APIError) -> ResponseReturnValue:
        return {"code": error.code}, error.status_code
```

!!! note

    The type ignore comment is required to work around a Quart bug.
