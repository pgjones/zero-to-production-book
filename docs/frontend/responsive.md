We should develop the frontend pages to look good on a mobile sized
screen. This is best done by activating developer tools in the browser
and using the device toolbar to select a mobile framing. This should
ensure that the app looks good from mobile up. However to ensure it
looks good on much larger screens we'll contain the content up to the
Material-UI `md` breakpoint by adding the following to
`frontend/src/App.tsx`,

```typescript
import Container from "@material-ui/core/Container";
...

const App = () => {
  ...
  return (
    ...
      <Container maxWidth="md">
        <Router />
      </Container>
  );
};
```
