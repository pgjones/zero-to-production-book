Many parts of the app, e.g. the routing, will need to know if the user
is currently logged in (authenticated). This is made available via a
React context, specifically an `AuthContext` as defined in
`frontend/src/AuthContext.tsx`, as,

```typescript
import React from "react";

interface IAuth {
  authenticated: boolean;
  setAuthenticated: (value: boolean) => void;
}

export const AuthContext = React.createContext<IAuth>({
  authenticated: true,
  setAuthenticated: (value: boolean) => {},
});

interface IProps {
  children?: React.ReactNode;
}

export const AuthContextProvider = ({ children }: IProps) => {
  const [authenticated, setAuthenticated] = React.useState(true);

  return (
    <AuthContext.Provider value={{ authenticated, setAuthenticated }}>
      {children}
    </AuthContext.Provider>
  );
};
```

which allows the authentication state to be used in any component via
a `useContext` hook,

```typescript
import { AuthContext } from "src/AuthContext";

...

const { authenticated } = React.useContext(AuthContext);
```

when setup and initialised by adding the following to `src/App.tsx`,

```typescript
...
import { AuthContextProvider } from "src/AuthContext";

const App = () => {
  ...
  return (
    <AuthContextProvider>
      ...
    </AuthContextProvider>
  );
}
```
